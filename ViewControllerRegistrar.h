//
//  ViewControllerRegistrar.h
//  ActividadesEnClase1_IOS
//
//  Created by Daniel Blas Rey on 12/4/16.
//  Copyright © 2016 Daniel Blas Rey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewControllerRegistrar : UIViewController
    IBOutlet UITextField *tfUser;
    IBOutlet UITextField *tfPassword;
    IBOutlet UILabel *lblQuestion;
    IBOutlet UIButton *btnLogIn;
    IBOutlet UIButton *btnRegistrate;
    IBOutlet UIImageView *image;
@end
