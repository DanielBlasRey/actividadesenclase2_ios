//
//  ViewControllerCollection.m
//  ActividadesEnClase1_IOS
//
//  Created by Daniel Blas Rey on 25/4/16.
//  Copyright © 2016 Daniel Blas Rey. All rights reserved.
//

#import "ViewControllerCollection.h"

@interface ViewControllerCollection ()

@end

@implementation ViewControllerCollection

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
