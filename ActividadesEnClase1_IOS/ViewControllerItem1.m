//
//  ViewControllerItem1.m
//  ActividadesEnClase1_IOS
//
//  Created by Daniel Blas Rey on 14/4/16.
//  Copyright © 2016 Daniel Blas Rey. All rights reserved.
//

#import "ViewControllerItem1.h"
#import "AppDelegate.h"
#import <Quickblox/Quickblox.h>

@interface ViewControllerItem1 ()

@end

@implementation ViewControllerItem1

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSMutableDictionary *getRequest = [NSMutableDictionary dictionary];
    
    [QBRequest objectsWithClassName:@"TablaCustomRequest" extendedRequest:getRequest successBlock:^(QBResponse *response, NSArray *objects, QBResponsePage *page) {
        // response processing
        nsObjetosDevueltos = objects;
        [tvMiTabla reloadData];
    } errorBlock:^(QBResponse *response) {
        // error handling
        NSLog(@"Response error: %@", [response.error description]);
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    int index = (int)indexPath.row;
    
    QBCOCustomObject *objetoEnLaFila = (QBCOCustomObject*) nsObjetosDevueltos[index];
    
    NSString *ciudad = [objetoEnLaFila.fields objectForKey:@"Ciudad"];
    float latitud = [[objetoEnLaFila.fields objectForKey:@"Latitud"] doubleValue];
    float longitud = [[objetoEnLaFila.fields objectForKey:@"Longitud"] doubleValue];
    TableViewCellItem1 *cell = (TableViewCellItem1*)[tableView dequeueReusableCellWithIdentifier:@"table1cell"];
    
    NSString *slat=[NSString stringWithFormat:@"    %@ | %f | %f",ciudad,latitud,longitud];
    [cell setTexto: slat];
    
    return cell;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return nsObjetosDevueltos.count;
}

@end
