//
//  ViewControllerItem1.h
//  ActividadesEnClase1_IOS
//
//  Created by Daniel Blas Rey on 14/4/16.
//  Copyright © 2016 Daniel Blas Rey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableViewCellItem1.h"


@interface ViewControllerItem1 : UIViewController <UITableViewDelegate, UITableViewDataSource>{
    NSInteger *num;
    IBOutlet UITableView *tvMiTabla;
    NSArray *nsObjetosDevueltos;
}

@end
