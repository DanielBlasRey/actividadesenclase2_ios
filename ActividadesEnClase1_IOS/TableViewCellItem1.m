//
//  TableViewCellItem1.m
//  ActividadesEnClase1_IOS
//
//  Created by Daniel Blas Rey on 14/4/16.
//  Copyright © 2016 Daniel Blas Rey. All rights reserved.
//

#import "TableViewCellItem1.h"
#import "AppDelegate.h"

@implementation TableViewCellItem1

- (void)awakeFromNib {
 
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    
}

-(void)setTexto:(NSString*)texto {
    lblTexto.text = texto;
}

@end
