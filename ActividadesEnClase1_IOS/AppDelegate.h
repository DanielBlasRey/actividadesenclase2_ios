//
//  AppDelegate.h
//  ActividadesEnClase1_IOS
//
//  Created by Daniel Blas Rey on 11/4/16.
//  Copyright © 2016 Daniel Blas Rey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "User.h"
#import <Quickblox/Quickblox.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate> {
    User *user;
}


-(User*)getUser;

@property (strong, nonatomic) UIWindow *window;


@end

