//
//  ViewControllerMapViewController.m
//  ActividadesEnClase1_IOS
//
//  Created by Daniel Blas Rey on 21/4/16.
//  Copyright © 2016 Daniel Blas Rey. All rights reserved.
//

#import "ViewControllerMap.h"
#import <MapKit/MapKit.h>
#import <Quickblox/Quickblox.h>
#import "MapPin.h"

@interface ViewControllerMap ()

@end

@implementation ViewControllerMap

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSMutableDictionary *getRequest = [NSMutableDictionary dictionary];
    
    [QBRequest objectsWithClassName:@"TablaCustomRequest" extendedRequest:getRequest successBlock:^(QBResponse *response, NSArray *objects, QBResponsePage *page) {
        // response processing
        for (int i = 0; i < objects.count; i++) {
            QBCOCustomObject *objetoEnLaFila = (QBCOCustomObject*) objects[i];
            NSString *ciudad = [objetoEnLaFila.fields objectForKey:@"Ciudad"];
            float latitud = [[objetoEnLaFila.fields objectForKey:@"Latitud"] doubleValue];
            float longitud = [[objetoEnLaFila.fields objectForKey:@"Longitud"] doubleValue];

            CLLocationCoordinate2D coord = {.latitude = latitud, .longitude = longitud};
            
            MapPin *pin = [[MapPin alloc] initWithCoordinate:coord];
            //pin.subtitle = geodata.status;
            pin.title = ciudad;
            
            [mapa addAnnotation:pin];
        }
    } errorBlock:^(QBResponse *response) {
        // error handling
        NSLog(@"Response error: %@", [response.error description]);
    }];

    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}



@end
