//
//  ViewControllerRegistrar.m
//  ActividadesEnClase1_IOS
//
//  Created by Daniel Blas Rey on 12/4/16.
//  Copyright © 2016 Daniel Blas Rey. All rights reserved.
//

#import "ViewControllerRegistrar.h"
#import "AppDelegate.h"


@interface ViewControllerRegistrar ()

@end

@implementation ViewControllerRegistrar



- (AppDelegate *)getAppDelegate
{
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}

-(IBAction)accionRegistrar:(id)sender {
    /*if (tfPassword.text.length >= 8) {
        AppDelegate *aDeleg = [self getAppDelegate];
        User *userTemp = [aDeleg getUser];
        [userTemp setMail:tfEmail.text];
        [userTemp setName:tfUser.text];
        [userTemp setPass:tfPassword.text];
     
    }*/
   
QBUUser *user = [QBUUser user];
user.password = tfPassword.text;
user.login = tfUser.text;
user.email = tfEmail.text;

if (tfPassword.text.length >= 8) {
    [QBRequest signUp:user successBlock:^(QBResponse *response, QBUUser *user) {
        [self performSegueWithIdentifier:@"RegistertoLogIn" sender:self];
    }
    errorBlock:^(QBResponse *response) {
        
    }];
    
 }
}


@end
