//
//  ViewControllerMapViewController.h
//  ActividadesEnClase1_IOS
//
//  Created by Daniel Blas Rey on 21/4/16.
//  Copyright © 2016 Daniel Blas Rey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface ViewControllerMap : UIViewController {
    IBOutlet MKMapView *mapa;
    NSArray *arrayMapa;
}

@end
